---
layout: post
title:  Ten Pro Tips for Jekyll 
date:   April 16, 2019
author: Jay
categories: jekyll update
description: Jtheme documentation #for meta description
comments: true
excerpt_separator: <!--more-->
---

Congratulations, you just downloaded Jekyll and you are looking on how to use it.
Jekyll is a powerful static site generator that has a lot of really cool uses.
Here is a list I compiled of uses for Jekyll you might have overlooked.
<!--more-->

## 10. Static Website

Jekyll is known to be a powerful blogging engine, but what is often overlooked, is Jekyll's use 
for building plain websites. When you code a static website by hand (no generators or pre-processing languages)
you end up having to copy and paste much of your code. Sometimes you forget to copy the right thing, or you don't make a necessary change that breaks the page. With Front mattter and Liquid, you only need to code something once and reference it throughout the site. This not only speeds up development, it also reduces the chance of "stupid" mistakes.

## 9. Automatic Sitemap

Plugins really expand the functionality of Jekyll. One of my favorites is the `jekyll-sitemap` plugin. Generating sitemaps by hand is tedious to say the least. With jekyll sitemap, all you have to do is reference in the gemfile, and it automatically generates a sitemap that is current with your site. So head on over and add it to your site. I relatively painless way to boost your website's SEO.


## 8. Make Your Site Modular

A pet peeve of mine is when a jekyll site or theme is when they have wierd includes and the layouts are filled with code. Jekyll's power is in its modularity. Take advantage of it! Even if it seems weird. When you are starting out designing the page, you don't know what you want or what you will end up throwing out. A highly modular design fixes this by encouraging wild experimentation and can speed up the desing process. In addition, a modular design makes it easier if you want a collaborator or optimizer.

## 7. Add a sidebar

Building on the previous point of being modular, consider giving your blog or website a sidebar. Sidebars not only provide consistent useful content to your visitors, they also help you add more relevant keywords that can help you get ranked higher. 

## 6. Create an Archive

Archives are an excellent module to include in large blogs. Guests can browse a short list of the posts instead of 
scrolling through the entire blog. While more difficult to setup archives can be leveraged to decrease bounce rate.

## 5. Pagination

Another important feature to add when your blog grows is pagination. Pagination breaks up your post listing into bite-sized chunks. This makes it easier for the users to find posts and decreases the page loading speed. 

## 4. Add a Search Bar

Search bars can be a great replacement for archives as they allow the user to quickly find articles and posts they are looking for. Setting up an affective search algorithm can be difficult (just ask Google). I found this [tutorial for a Jekyll searchbar](https://blog.webjeda.com/instant-jekyll-search/) to be rather useful. It takes some tinkering, but once setup, it can be very useful.

## 3. Create a Collection

The backbone of Jekyll is posts. You can list posts, organize posts, give posts categories. However, did you know that posts belong to a tool called collections? Create collections by adding this snippet of code:

<pre>
collections: 
  example_name:
    output: true
</pre>

you declare the name of the collection. Simply make a new file with that collection name and use that name instead of post in liquid. For example instead of `site.posts` is would be `site.example_name`. 


## 2. Load Scripts Conditionally

This is one of my favorite tips. Often times you will have JS libraries or scripts that you only need in a few pages. However, since the scripts are in the footer, you end up loading a useless JS script on every page. Needless to say this is very inefficient. The good news is that it is a very easy problem to fix. 
Encase your optional scripts in {% raw %} `{% if page.x %}` and `{% endif %}`{% endraw %}. Now whenever you want to use those scripts just announce in front matter `x: true`. This will greatly speed up you webpage.

## 1. Removing Render Blocking Resources

Have you ever ran your website through a speed validator and it gave you the error `render blocking resources`.
Here is how you can fix it :D! 

To solve this error is simple. It is being generated because your css files are in the <head> tag. This means that 
the page will not load unless those CSS files are loaded. Thus all you have to do is place your CSS code in `<style>` tags just below the <head>. Easy right? Well here is the catch, assuming you want the CSS to be used in other files, you will have to constantly be copying and pasting code. Here comes the good part. With Jekyll you get the benefits of placing your code in `<style>` tags without being a render blocking resource. How is this feat possible? you might ask.

Put your CSS file in an include with a .css extension. Now, insert that between style tags with {% raw %}`{% include x.css %}`{% endraw %}.
Mind blowing right? You can edit the code and treat it like a separate file while it is simulteneously being rendered as one file. The only downside to this approach is the a pre-processing languages like SASS do not compile. Other than that, this approach has speeded up many of my webpages.


Share this post if you found it useful. What are your thoughts? What did I miss? 